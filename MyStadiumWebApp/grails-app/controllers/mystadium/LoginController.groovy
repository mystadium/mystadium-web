package mystadium


class LoginController {

    def index() {
        def userId = session.getAttribute("userId")
        def userNom = session.getAttribute("userNom")
        [nom : userNom , id : userId]
    }
    def login(){
        if(request.method == 'POST'){
            def user = Utilisateur.findByLoginAndMotDePasse(params.login, params.password.encodeAsSHA1())

            if(user != null){
                session.setAttribute("userId",user.id)
                session.setAttribute("userNom",user.nom)
                redirect(controller:"login" , action:"index")
            }else{
                [error: 'Login ou mot de passe incorrect']
            }
        }
        [error : null]
    }
}
