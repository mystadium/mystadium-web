<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>MyStadium Login</title>
</head>
<body>
<div id="content" role="main">
    <section class="row colset-2-its">
        <g:form action="login">
            <g:hasErrors bean="${utilisateur}">
                <div class="errors">
                    <g:renderErrors bean="${utilisateur}" />
                </div>
            </g:hasErrors>

            <p>
                <label for="login"> Login</label>
                <br/>
                <g:textField name="login" class="${hasErrors(bean:user,field: 'login', 'errors')}" />

            </p>
            <p>
                <label for="password">Password</label>
                <br/>
                <g:textField name="password" class="${hasErrors(bean:user,field: 'password', 'errors')}" />
            </p>
            <p class="button">
                <g:submitButton name="submitButton" value="Connexion" class="button"></g:submitButton>
            </p>
        </g:form>


    </section>
</div>
</body>
</html>